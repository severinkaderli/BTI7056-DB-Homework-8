---
title: "BTI7056-DB"
subtitle: "Homework 8"
author:
    - Severin Kaderli
    - Marius Schär
extra-info: true
institute: "Berner Fachhochschule"
department: "Technik und Informatik"
lecturer: "Dr. Kai Brünnler"
lang: "de-CH"
toc: false
rule-color: 00c0c0
link-color: 00c0c0
...

# Aufgabe 1
> Realisieren Sie folgende Aufgaben in publications.db

> 1. Bestimmen Sie für jeden Laden und jeden Autor den Gesamtpreis der von
> diesem Autor in diesem Laden verkauften Bücher (ohne Discounts),
> absteigend sortiert.

```{.sql .numberLines}
SELECT au.au_lname,
       au.au_fname,
       sto.stor_name,
       sum(ti.price * sd.qty)
FROM stores AS sto
JOIN salesdetail AS sd ON sto.stor_id = sd.stor_id
JOIN titles AS ti ON sd.title_id = ti.title_id
JOIN titleauthor AS ta ON ta.title_id = ti.title_id
JOIN authors AS au ON au.au_id = ta.au_id
GROUP BY au.au_id,
         sto.stor_id;
```

\newpage

> 2. Bestimmen Sie alle Autoren die weniger 'psychology ' Bücher herausgegeben
> haben als 'Livia Karsen'.

```{.sql .numberLines}
WITH au_type_cnt AS
  (SELECT authors.au_id AS id,
          authors.au_fname || ' ' || authors.au_lname AS name,
          titles.type,
          count(1) AS cnt
   FROM authors
   NATURAL JOIN titleauthor
   NATURAL JOIN titles
   GROUP BY authors.au_id,
            titles.type)
SELECT *
FROM au_type_cnt
WHERE (TYPE LIKE 'psychology  '
       AND cnt <
         (SELECT cnt
          FROM au_type_cnt
          WHERE name LIKE 'Livia Karsen'
            AND TYPE LIKE 'psychology' ))
  OR TYPE NOT LIKE 'psychology  '
GROUP BY id
ORDER BY name ASC;
```

> 3. Geben Sie für jedes Buch seinen Titel, Preis, Anzahl bisher verkaufter
> Exemplare, sowie die effektiv geschuldeten Tantiemen an. (Die pro Buch
> geschuldete Tantieme in Prozent des Verkaufspreises ist in der Spalte
> royalty in Abhängigkeit von den Verkaufszahlen angegeben.)

```{.sql .numberLines}
SELECT title,
       price,
       total_sales,
       CASE
           WHEN price * total_sales * royalty <= lorange THEN lorange
           WHEN price * total_sales * royalty >= hirange THEN hirange
           ELSE price * total_sales * royalty
       END AS royalties
FROM titles
NATURAL JOIN roysched;
```

\newpage
> 4. Testen Sie, ob die Werte des Attributs `total_sales` korrekt sind,
> indem Sie alle Buchtitel mit falschen Werten zusammen mit den Werten
> ausgeben.

```{.sql .numberLines}
SELECT title,
       total_sales,
       sum(qty) AS actual_sales
FROM titles
NATURAL JOIN salesdetail
GROUP BY title_id
HAVING total_sales <> actual_sales;
```

> 5. Geben Sie die Läden an, die schon Bücher aller Typen verkauft haben.

```{.sql .numberLines}
SELECT stor_id,
       stor_name
FROM stores
WHERE stor_id NOT IN
    (SELECT stor_id
     FROM
       (SELECT DISTINCT stor_id, TYPE
        FROM titles
        CROSS JOIN stores
        EXCEPT SELECT DISTINCT stor_id, TYPE
        FROM salesdetail
        JOIN titles ON titles.title_id = salesdetail.title_id));
```

# Aufgabe 2
> Realisieren Sie folgende Aufgaben in university.db

> 1. Finde alle Kursdurchführungen, die miteinander in Konflikt stehen, d.h.
> die dazu führen, dass ein Dozent zur gleichen Zeit an unterschiedlichen
> Orten sein muss.

```{.sql .numberLines}
WITH schedule AS
  (SELECT *
   FROM SECTION AS se
   JOIN teaches te ON se.course_id = te.course_id
   AND se.sec_id = te.sec_id
   JOIN instructor ins ON te.ID = ins.ID)
SELECT *
FROM schedule AS alpha
CROSS JOIN schedule AS beta
WHERE alpha.ID = beta.ID
  AND alpha.year = beta.year
  AND alpha.time_slot_id = beta.time_slot_id
  AND NOT (alpha.building = beta.building
           AND alpha.room_number = beta.room_number)
```

\newpage
> 2. Verhindern Sie mithilfe von Triggern, dass miteinander in Konflikt
> stehende Kursdurchführungen eingetragen werden.

```{.sql .numberLines}
CREATE TRIGGER check_for_conflicts
BEFORE
INSERT ON section
BEGIN
  SELECT CASE
    WHEN
      (SELECT count(*)
        FROM section
        WHERE course_id=NEW.course_id
          AND sec_id=NEW.sec_id
          AND semester=NEW.semester
          AND YEAR=NEW.year
          AND building=NEW.building
          AND room_number=NEW.room_number
          AND time_slot_id=NEW.time_slot_id) > 0
      THEN
        RAISE (ABORT, "Conflict")
  END;
END;
```
